Source code for the [Urban Circulation Conference 2022 website](https://kritis.gitlab.io/ucconf2022/), hosted by the [Research Training Group KRITIS](https://www.kritis.tu-darmstadt.de/).

Web design informed by current best practices for [Resilient Web Design](https://resilientwebdesign.com/).

Web page styling based on the pretty rad [Bulma CSS framework](https://www.bulma.io/).

Serverless web form functionality provided by [BASIN](https://www.usebasin.com/).
